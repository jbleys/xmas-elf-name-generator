<?php
/*
Plugin Name:  Xmas Elf Name Generator
Plugin URI:   https://bitbucket.org/jbleys/xmas-elf-name-generator
Description:  Simply add shortcode <strong>[xmas_elf_name_generator]</strong> to your content for a jolly elf name generator. A page or post can only have one [xmas_elf_name_generator].
Author:       Jasper Bleijs
Author URI:   
Version:      0.2.5
License:      GPLv2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  
Domain Path:  
Bitbucket Plugin URI:  https://bitbucket.org/jbleys/xmas-elf-name-generator

Xmas Elf Name Generator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Xmas Elf Name Generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Xmas Elf Name Generator. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */
?>
<?php
if ( !class_exists( 'XmasElfNameGenerator' ) ) {

  class XmasElfNameGenerator {

    // class initializaton
    public static function init() {
      wp_enqueue_style( 'xmas-elf-name-generator-google-fonts', 'https://fonts.googleapis.com/css?family=Mountains+of+Christmas:400,700', false ); 
      wp_enqueue_style( 'xmas_elf_name_generator', plugin_dir_url(__FILE__) . 'xmas-elf-name-generator.css' );
      wp_register_script('xmas_elf_name_generator', plugin_dir_url(__FILE__) . 'xmas-elf-name-generator.js');
      wp_enqueue_script('xmas_elf_name_generator');
      add_shortcode('xmas_elf_name_generator', array( __CLASS__, 'elfname_generator_html' ));
    }
    // html
    function elfname_generator_html() {
      $btnclass = "xmas-elf-name-generator";
      $theme = wp_get_theme();
      if ( 'UW 2014' == $theme->name || 'UW 2014' == $theme->parent_theme ) { $btnclass = "btn btn-primary small"; }
      // add each line of html to a variable
      $ln01 = '<h2>Xmas Elf Name Generator</h2>';
      $ln02 = '<div class="xmas-elf-name-generator-wrapper">';
      $ln03 = ' <div id="xmas-elf-name-generator__display" aria-label="Displays an elf name"></div>';
      $ln04 = '</div>';
      $ln05 = '<p><button type="button" id="xmas-elf-name-generator__generate" class="' . $btnclass . '" aria-controls="xmas-elf-name-generator__display">Get Another Elf Name</button></p>';
      $ln06 = '<details hidden aria-hidden="true" style="display: none;">';
      $ln07 = '  <summary>Info:</summary>';
      $ln08 = '  <p id="xmas-elf-name-generator__info"></p>';
      $ln09 = '</details>';
      return sprintf("{$ln01}{$ln02}{$ln03}{$ln04}{$ln05}{$ln06}{$ln07}{$ln08}{$ln09}");
    }

  }

  XmasElfNameGenerator::init();

}