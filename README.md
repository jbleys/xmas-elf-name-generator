# README #

This README document contains steps necessary to use the _Xmas Elf Name Generator_ WordPress plugin

## Description ##

* Add a jolly elf name generator to your content. Simply add shortcode __[xmas_elf_name_generator]__ anywhere in your content.

## How do I get set up? ##

* Navigate to your wp-content/plugins/ folder
* `git clone https://bitbucket.org/jbleys/xmas-elf-name-generator`

## Version ##

* 0.2.4

## Who do I talk to? ##

* jbleys@yahoo.com