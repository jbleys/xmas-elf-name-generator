function XmasElfNameGenerator () {
  
  XmasElfNameGenerator.prototype.firstNames = ["Acorn","Alabaster","Angel","Berry","Bing","Bling","Blitz","Blue","Bluebell","Brandysnap","Brownie","Buddy","Bushy","Buster","Butters","Button","Buttons","Candycane","Cedar","Chestnut","Choco","Cinnamon","Coco","Cocoa","Cookie","Dash","Elm","Evergreen","Fig","Figgy","Fir","Fizzy","Flake","Fluffy","Frost","Frosty","Fruity","Fudge","Fuzzle","Garland","Ginger","Gingernuts","Gingersnap","Glitter","Glory","Hazelnut","Ice","Jangle","Jingle","Jolly","Marzipan","Merry","Mince","Mint","Mistle","Mistletoe","Noel","Nutmeg","Pepper","Peppermint","Perky","Pine","Pinecone","Pinetree","Pudding","Rusty","Shimmer","Skittle","Snappy","Snow","Snowball","Snowdrop","Snowflake","Sparkle","Sprinkle","Sprinkles","Starlight","Stripes","Sugar","Sugarplum","Tinkles","Tiny","Topper","Trinket","Twinkle","Twinkletoes","Wink","Winter","Yule","Belle","Brandy","Bubbles","Candy","Carol","Cherry","Clove","Cupcake","Dandy","Ember","Emerald","Eve","Evie","Faith","Gloria","Hazel","Holly","Honey","Honeycomb","Hope","Ivy","Jewel","Joy","Juniper","Noelle","Ruby","Scarlet","Trixie","Krampus","Dizzy","Aqua","Awkward","Blizzard","Bells","Eggnog","Carols","Fruitcake","Greenfir","Hollyberry","Jollyfig","Jackfrost","Lollipop","Jubilee","Verysweet","Whimsy","Ribbons","Yodel","Toys","Longbottom","Snowbell","Hollyfir","Jelly","Ham","Giggle","Northstar","Cookies","Snowy","Rednose","Yummy","Stormy","Wonder","Miracle","Pompom","Max","Elvis","Cosmo","Gingerbread","Red","Goose","Kris","Poinsettia","Rudolph","Nicholas","Season","Scrooge","Star","Coal","Tickle","Sparky","Gilly","Mavis","Tink","Tinker","Molly","Toffeeapple","Giggley","Floppy","Licky","Tumbleflump","Fuzzy","Smiley","Smiles","Scrummy","Podgy","Fairy","Tummy","Bouncy","Lucky","Cuddly","Sneezy","Frankincense","Navidad","Goodwill","Happy","Partridge","Nick","Parsnip","Charlotte","Wintry","Antler","Rejoice","Toboggan","Comet","Cupid","Dancer","Dasher","Prancer","Prancy","Vixen","Cozy","Butterrum","Jollity","Grinchy","Pops","Gooey","Caramel","Buttercrunch","Cane","Popcorn","Dreidel","Hanukkah","Saucy","Zesty","Quirky","Cornucopia","Ebenezer","Donner","Olive","Cheery","Zippy","Pixie","Christy","Wishy","Pumpkin","Hoho","Pasty","Peaches","Nipper","Clause","Tootsie","Sunny","Spunky","Crumpet","Noddy","Snickerdoodle","Elfie","Tingle","Menorah","Knickknack","Rooftop","Naughty","Nice"];
  
  XmasElfNameGenerator.prototype.lastNamePrefixes = ["Angel","Bustle","Busy","Candle","Candy","Carol","Chill","Chilly","Chimney","Chocolate","Cider","Cookie","Crackle","Cuddle","Dream","Ever","Fire","Flippy","Frost","Frosty","Fruit","Gift","Good","Goody","Grotto","Happy","Holy","Holly","Hot","Hustle","Ivy","Jiggle","Jingle","Jolly","Magic","Milk","Milky","Miracle","Mistle","Mitten","Morning","Muffin","Nibble","Night","Nippy","Party","Pickle","Plum","Poem","Pudding","Rhyme","Ribbon","Sleepy","Snow","Sparkle","Sugar","Sweet","Toffee","Twinkle","Wiggle","Ice","Reindeer","Green","Glitter","Wonder","Wooly","Star","Yodel","Jelly","Giggle","Winter","Silver","Stripy","Tinsel","Ginger","Kringle","Season","Treacle","Bright","Christmas","Wrapping","Gobble","Rum","Butter","Caramel","Kwanzaa","Crackling","Pixie","Advent","Pumpkin","Stocking","Sparkley","Pointy","Floppy","Pecan","Crinkle","Twizzle","Glow","Tingly","Warm","Gold","Red","White"];
  
  XmasElfNameGenerator.prototype.lastNameSuffixes = ["ball","beard","bell","bow","box","cake","cane","card","carol","cheer","dance","dancer","dash","feast","flake","foot","friend","frost","fun","game","gift","glitter","glove","guest","hat","hope","hug","icicle","ivy","joke","joy","jump","kiss","laugh","light","love","milk","mitten","moon","myrrh","night","pie","plum","scarf","sledge","sleigh","song","spirit","star","toy","tree","warmth","wine","wish","wrap","storm","drops","wreath","jingle","socks","shine","sweater","jelly","bows","snaps","baubles","buns","crystals","pudding","stocking","lights","bubbles","trifle","fluff","pears","fir","bells","crackers","boughs","toe","punch","cider","eve","goose","sled","stuffing","tights","pants","buttons","fairy","helper","package","pickle","candle","turkey","prance","drizzle","boots","sauce","blitz","dolls","dress","stuffer","nose","hair","glogg","kissel","pins","ears","cookie","glow","tidings","tingles"];
  
  XmasElfNameGenerator.prototype.generate = function () {
    var numRnd1 = Math.floor(Math.random() * XmasElfNameGenerator.prototype.firstNames.length);
    var numRnd2 = Math.floor(Math.random() * XmasElfNameGenerator.prototype.lastNamePrefixes.length);
    var numRnd3 = Math.floor(Math.random() * XmasElfNameGenerator.prototype.lastNameSuffixes.length);
    var elfName = XmasElfNameGenerator.prototype.firstNames.sort()[numRnd1].toString() + " " + XmasElfNameGenerator.prototype.lastNamePrefixes.sort()[numRnd2].toString() + XmasElfNameGenerator.prototype.lastNameSuffixes.sort()[numRnd3].toString();
    return elfName;
  };
  
  XmasElfNameGenerator.prototype.numberOfCombinations = function () {
    var numLastNameCombos = (XmasElfNameGenerator.prototype.lastNamePrefixes.length * XmasElfNameGenerator.prototype.lastNameSuffixes.length);
    var numNameCombos = (XmasElfNameGenerator.prototype.firstNames.length * numLastNameCombos);
    return numNameCombos.toString();
  };
  
};

var generator = new XmasElfNameGenerator();

// Internet Explorer 6-11
var isIE = /*@cc_on!@*/false || !!document.documentMode;

// the DOM will be available here
document.addEventListener("DOMContentLoaded", function(event) {
  
  //continue if a generator wrapper exists
  if (document.getElementsByClassName("xmas-elf-name-generator-wrapper").length == 0)
    return;
  
  var wrapper = document.getElementsByClassName("xmas-elf-name-generator-wrapper")[0];
  var info = document.getElementById("xmas-elf-name-generator__info");
  var btn = document.getElementById("xmas-elf-name-generator__generate");
  var display = document.getElementById("xmas-elf-name-generator__display");
  
  //show number of combos
  info.appendChild(document.createTextNode('The Xmas Elf Name Generator has ' + generator.numberOfCombinations() + ' possible elf name combinations.'));
  
  // set initial elf name
  display.appendChild(document.createTextNode(generator.generate()));
  
  // click event
  btn.addEventListener("click", function () {
    // no animation for old IE browsers
    if ( isIE ) {
      while (display.firstChild) { display.removeChild(display.firstChild); }
      display.appendChild(document.createTextNode(generator.generate()));
      return;
    }
    
    display.className = "jingling";
    // set timeout to match the css transition time of .5 sec
    window.setTimeout(function () {
      // clear existing text
      while (display.firstChild) { display.removeChild(display.firstChild); }
      // generate new elf name
      display.appendChild(document.createTextNode(generator.generate()));
    }, 500);
  });
  
  // transitionend event
  display.addEventListener("transitionend", function(event) {
    display.classList.remove("jingling");    
  }, false);
  
});